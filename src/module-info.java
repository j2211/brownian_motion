module brownian {
    requires javafx.controls;
    requires javafx.fxml;

    requires org.kordamp.ikonli.javafx;
    requires org.kordamp.bootstrapfx.core;
    requires MaterialFX;
    requires commons.math3;

    opens main.controllers to javafx.fxml;
    exports main;
}