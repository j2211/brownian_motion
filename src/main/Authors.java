package main;

import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.Objects;

public class Authors extends Stage {

    public void open() {
        try {
            // load authors FXML view
            FXMLLoader fxmlLoader = new FXMLLoader(Main.class.getResource("/fxml/authors-view.fxml"));

            // setup scene
            Scene scene = new Scene(fxmlLoader.load(), 600, 400);
            this.setTitle("Autorzy");
            this.setScene(scene);

            // load CSS styles
            scene.getStylesheets().add(Objects.requireNonNull(getClass().getResource("/css/fonts.css")).toExternalForm());
            scene.getStylesheets().add(Objects.requireNonNull(getClass().getResource("/css/styles.css")).toExternalForm());

            this.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}