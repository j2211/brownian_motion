package main.models;

import javafx.geometry.Point2D;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

public class Particle {
    private Point2D position;
    private Point2D velocity;
    private Color color;
    private int radius;
    private int mass;

    private boolean isBigParticle;

    public Particle(Point2D position0) {
        this.isBigParticle = false; // small particle by default

        this.radius = 3;
        this.mass = 1;

        this.position = position0;
        this.velocity = Point2D.ZERO;

        this.color = Color.GRAY;
    }

    public void draw(GraphicsContext gc) {
        var r = this.getRadius();
        gc.setFill(this.getColor());
        gc.fillOval(this.position.getX() - r, this.position.getY() - r, r * 2, r * 2);
    }

    public void move() {
        this.setPosition(this.position.add(this.velocity));
    }

    public void collide(Particle other) {
        // calculating just the square to avoid Math.sqrt()
        var dx = this.position.getX() - other.getPosition().getX();
        var dy = this.position.getY() - other.getPosition().getY();
        var distance = dx * dx + dy * dy;
        var sumOfRadii = this.radius + other.getRadius();

        // if collision occurred
        if (this != other && distance <= sumOfRadii * sumOfRadii) {
            var m1 = this.getMass();
            var m2 = other.getMass();
            double M = m1 + m2;

            var normal = this.position.subtract(other.getPosition()).normalize();

            var relVel = this.velocity.subtract(other.getVelocity()).dotProduct(normal);

            if (relVel < 0) {
                this.setVelocity(this.velocity.subtract(normal.multiply(2 * m2 / M * relVel)));
                other.setVelocity(other.velocity.add(normal.multiply(2 * m1 / M * relVel)));
            }
        }
    }

    public void negateVelX() {
        this.setVelocity(new Point2D(-this.velocity.getX(), this.velocity.getY()));
    }

    public void negateVelY() {
        this.setVelocity(new Point2D(this.velocity.getX(), -this.velocity.getY()));
    }

    public int getRadius() {
        return radius;
    }

    public void setRadius(int radius) {
        this.radius = radius;
    }

    public int getMass() {
        return mass;
    }

    public void setMass(int mass) {
        this.mass = mass;
    }

    public Point2D getPosition() {
        return position;
    }

    public void setPosition(Point2D position) {
        this.position = position;
    }

    public Point2D getVelocity() {
        return velocity;
    }

    public void setVelocity(Point2D velocity) {
        this.velocity = velocity;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }
    public boolean isBigParticle() {
        return isBigParticle;
    }

    public void setBigParticle(boolean bigParticle) {
        isBigParticle = bigParticle;
    }

}
