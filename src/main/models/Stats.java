package main.models;

import javafx.beans.property.SimpleDoubleProperty;

import java.util.HashMap;
import java.util.LinkedHashMap;

public class Stats {
    public static final String numOfBigParticles = "Liczba pyłków";
    public static final String totalDistanceTravelled = "Pokonany dystans (pyłki)";
    public static final String averageSpeedBig = "Średnia prędkość (pyłki)";
    public static final String averageSpeedSmall = "Średnia prędkość (cząsteczki)";
    private final LinkedHashMap<String, SimpleDoubleProperty> stats;

    public Stats() {
        this.stats = new LinkedHashMap<>();

        this.stats.put(Stats.numOfBigParticles, new SimpleDoubleProperty(0));
        this.stats.put(Stats.totalDistanceTravelled, new SimpleDoubleProperty(0));
        this.stats.put(Stats.averageSpeedBig, new SimpleDoubleProperty(0));
        this.stats.put(Stats.averageSpeedSmall, new SimpleDoubleProperty(0));
    }

    public SimpleDoubleProperty get(String key) {
        return stats.get(key);
    }

    public void update(String key, double newValue) {
        this.stats.get(key).setValue(newValue);
    }

    public void reset() {
        for (String key : this.stats.keySet()) {
            this.update(key, 0);
        }
    }

    public HashMap<String, SimpleDoubleProperty> getStats() {
        return stats;
    }
}
