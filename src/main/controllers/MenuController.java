package main.controllers;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.control.MenuBar;
import main.Authors;
import main.Options;

public class MenuController {
    public MenuBar menubar;

    @FXML
    protected void openSettings() {
        Options options = new Options();
        options.open();
    }

    @FXML
    protected void closeProgram() {
        Platform.exit();
    }

    @FXML
    protected void openAuthors() {
        System.out.println("open main.Authors!");
        var authors = new Authors();
        authors.open();
    }

}