package main.controllers;

import io.github.palexdev.materialfx.controls.MFXToggleButton;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;

public class OptionsController {

    @FXML
    protected void changeMode(ActionEvent e) {
        MFXToggleButton button = (MFXToggleButton) e.getSource();
        if (button.isSelected()) {
            System.out.println("Dark Mode");
        }
        if (!button.isSelected()) {
            System.out.println("Light Mode");
        }
    }

    @FXML
    protected void langPL() {
        System.out.println("Polish");
    }

    @FXML
    protected void langEN() {
        System.out.println("English");
    }


}