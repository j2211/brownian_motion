package main.controllers;

import io.github.palexdev.materialfx.controls.MFXButton;
import io.github.palexdev.materialfx.controls.MFXSlider;
import javafx.beans.binding.Bindings;
import javafx.beans.binding.BooleanBinding;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import main.components.CanvasPane;

import java.net.URL;
import java.util.ResourceBundle;

public class MainController implements Initializable {
    private static final CanvasPane canvasPane = new CanvasPane();

    @FXML
    private MFXButton addBigParticleButton;

    @FXML
    private MFXButton removeBigParticleButton;

    @FXML
    private MFXSlider numberOfParticlesSlider;

    @FXML
    private MFXSlider tempSlider;

    @FXML
    private Label tempLabel;

    @FXML
    private BorderPane container;

    @FXML
    private void addBigParticle() {
        canvasPane.addBigParticle();
    }

    @FXML
    private void removeBigParticle() {
        canvasPane.removeBigParticle();
    }

    @FXML
    private void resetCanvas() {
        canvasPane.resetCanvas();
    }

    @FXML
    private void startSimulation() {
        canvasPane.startTimer();
    }

    @FXML
    private void stopSimulation() {
        canvasPane.stopTimer();
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        container.setCenter(canvasPane);

        // handle particles slider
        canvasPane.setNumberOfParticles((long) numberOfParticlesSlider.getValue());
        canvasPane.setTemperature((int) tempSlider.getValue());
        tempLabel.setText((int) tempSlider.getValue() + " K");

        numberOfParticlesSlider.valueProperty().addListener((observable, oldValue, newValue) -> {
            canvasPane.setNumberOfParticles(newValue.intValue());
            canvasPane.generateParticles();
        });

        tempSlider.valueProperty().addListener((observable, oldValue, newValue) -> {
            canvasPane.setTemperature(newValue.intValue());
            canvasPane.generateParticles();
            tempLabel.setText((int) tempSlider.getValue() + " K");
        });

        BooleanBinding timerBind = Bindings.createBooleanBinding(() -> !canvasPane.timerRunning.getValue(), canvasPane.timerRunning);
        addBigParticleButton.disableProperty().bind(timerBind);
        removeBigParticleButton.disableProperty().bind(timerBind);
    }
}