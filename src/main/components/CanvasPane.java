package main.components;

import javafx.animation.AnimationTimer;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.geometry.Point2D;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Color;
import main.MB;
import main.models.Particle;
import main.models.Stats;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

import static javafx.scene.paint.Color.WHITE;

public class CanvasPane extends AnchorPane {
    private final Canvas canvas;
    private final List<Particle> bigParticles = new ArrayList<>();
    private final List<Particle> particles = new ArrayList<>();
    private final GraphicsContext gc;
    private final AnimationTimer timer;
    private final Stats stats;
    public BooleanProperty timerRunning = new SimpleBooleanProperty(false);
    private long numberOfParticles;
    private int temperature;

    public CanvasPane() {
        this.canvas = new Canvas(400, 400);
        this.getChildren().add(canvas);

        this.gc = canvas.getGraphicsContext2D();

        // bind size of canvas to outer pane container
        canvas.widthProperty().bind(this.widthProperty());
        canvas.heightProperty().bind(this.heightProperty());

        // redraw canvas when size changes.
        canvas.widthProperty().addListener(event -> this.drawCanvas());
        canvas.heightProperty().addListener(event -> this.drawCanvas());
        this.drawCanvas();

        var statsPane = new StatsPane();
        stats = statsPane.getStats();

        this.getChildren().add(statsPane);
        AnchorPane.setBottomAnchor(statsPane, 20.0);
        AnchorPane.setRightAnchor(statsPane, 20.0);

        timer = new AnimationTimer() {
            @Override
            public void handle(long l) {
                CanvasPane.this.onUpdate();
            }
        };
    }

    private void drawCanvas() {
        gc.setFill(WHITE);
        gc.fillRect(0, 0, canvas.getWidth(), canvas.getHeight());
    }

    public void generateParticles() {
        particles.clear();
        MB mb = new MB(temperature);

        for (Particle bigParticle : bigParticles)
            bigParticle.setVelocity(Point2D.ZERO);

        for (long i = 0; i < this.getNumberOfParticles(); i++) {
            var position0 = new Point2D(Math.random() * canvas.getWidth(), Math.random() * canvas.getHeight());
            var particle = new Particle(position0);
            mb.generateVelocity();
            particle.setVelocity(new Point2D(mb.setVx() / 100, mb.setVy() / 100));
            particles.add(particle);
        }
    }

    public void addBigParticle() {
        var bigParticle = new Particle(new Point2D(Math.random() * canvas.getWidth(),
                Math.random() * canvas.getHeight()));
        bigParticle.setRadius(25);
        bigParticle.setMass(222 / 40); //Radon
        bigParticle.setColor(Color.color(Math.random(), Math.random(), Math.random()));
        bigParticle.setBigParticle(true);
        bigParticles.add(bigParticle);

        var currentNumberOfBigParticles = stats.getStats().get(Stats.numOfBigParticles).intValue();
        stats.update(Stats.numOfBigParticles, currentNumberOfBigParticles + 1);
    }

    public void removeBigParticle() {
        // remove first big particle from the list (FIFO)
        if (bigParticles.size() > 0) {
            bigParticles.remove(0);

            var currentNumberOfBigParticles = stats.getStats().get(Stats.numOfBigParticles).intValue();
            stats.update(Stats.numOfBigParticles, currentNumberOfBigParticles - 1);
        }
    }

    private void onUpdate() {
        this.drawCanvas();

        if (particles.isEmpty())
            this.generateParticles();

        var allParticles = Stream.concat(particles.stream(), bigParticles.stream()).toList();

        allParticles.forEach(particle -> {
            var oldPosition = particle.getPosition();

            particle.move();

            var newPosition = particle.getPosition();

            // log total distance travelled by all big particles
            if (particle.isBigParticle())
                stats.update(
                        Stats.totalDistanceTravelled,
                        stats.get(Stats.totalDistanceTravelled).doubleValue() + newPosition.distance(oldPosition)
                );

            var x = newPosition.getX();
            var y = newPosition.getY();

            var r = particle.getRadius();

            // wall collisions
            var sideX = x - canvas.getWidth() / 2;
            var sideY = y - canvas.getHeight() / 2;

            if (sideX * particle.getVelocity().getX() > 0 && (x - r < 0 || x + r > canvas.getWidth()))
                particle.negateVelX();
            if (sideY * particle.getVelocity().getY() > 0 && (y - r < 0 || y + r > canvas.getHeight()))
                particle.negateVelY();

            // collisions between small particles
            for (Particle otherParticle : allParticles) {
                particle.collide(otherParticle);
            }

            particle.draw(gc);
        });

        double sumVelocitySmall = 0;
        double sumVelocityBig = 0;

        for (Particle particle : allParticles) {
            var velocityMagnitude = particle.getVelocity().magnitude();

            if (particle.isBigParticle()) sumVelocityBig += velocityMagnitude;
            else sumVelocitySmall += velocityMagnitude;
        }
        stats.update(Stats.averageSpeedSmall, sumVelocitySmall / (double) particles.size());
        stats.update(Stats.averageSpeedBig, sumVelocityBig / (double) bigParticles.size());
    }

    public void resetCanvas() {
        this.stopTimer();
        this.bigParticles.clear();
        this.particles.clear();
        this.drawCanvas();

        this.stats.reset();
    }

    public void startTimer() {
        timer.start();
        this.timerRunning.set(true);
    }

    public void stopTimer() {
        timer.stop();
        this.timerRunning.set(false);
    }

    public long getNumberOfParticles() {
        return numberOfParticles;
    }

    public void setNumberOfParticles(long numberOfParticles) {
        this.numberOfParticles = numberOfParticles;
    }

    public void setTemperature(int temperature) {
        this.temperature = temperature;
    }

}
