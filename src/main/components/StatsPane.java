package main.components;

import javafx.beans.binding.Bindings;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import main.models.Stats;

public class StatsPane extends VBox {
    private final Stats stats = new Stats();

    public StatsPane() {
        this.setPrefWidth(300);
        this.getStyleClass().add("stats-container");

        var statsTitle = new Label("Statystyki:");
        statsTitle.getStyleClass().add("stats-title");
        this.getChildren().add(statsTitle);

        for (String key : stats.getStats().keySet()) {
            var row = new HBox();

            var keyLabel = new Label(key);
            row.getChildren().add(keyLabel);

            var value = stats.get(key);
            var valueLabel = new Label(Double.toString(value.intValue()));
            valueLabel.textProperty().bind(Bindings.format("%.3f", value));
            valueLabel.setMaxWidth(Double.MAX_VALUE);
            valueLabel.setAlignment(Pos.CENTER_RIGHT);

            keyLabel.setMaxWidth(Double.MAX_VALUE);
            valueLabel.setMaxWidth(Double.MAX_VALUE);
            HBox.setHgrow(keyLabel, Priority.ALWAYS);
            HBox.setHgrow(valueLabel, Priority.ALWAYS);

            row.getChildren().add(valueLabel);
            this.getChildren().add(row);
        }
    }

    public Stats getStats() {
        return stats;
    }
}
