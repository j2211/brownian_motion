package main;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.Objects;

public class Main extends Application {
    public static void main(String[] args) {
        launch();
    }

    @Override
    public void start(Stage stage) throws IOException {
        // load main FXML view
        FXMLLoader fxmlLoader = new FXMLLoader(Main.class.getResource("/fxml/main-view.fxml"));

        // setup scene
        Scene scene = new Scene(fxmlLoader.load(), 1024, 720);
        stage.setTitle("Model ruchów Browna - v1.0");
        stage.setScene(scene);
        stage.setResizable(false);

        // load CSS styles
        scene.getStylesheets().add(Objects.requireNonNull(getClass().getResource("/css/fonts.css")).toExternalForm());
        scene.getStylesheets().add(Objects.requireNonNull(getClass().getResource("/css/styles.css")).toExternalForm());

        stage.show();
    }
}