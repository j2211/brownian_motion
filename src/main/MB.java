package main;

import org.apache.commons.math3.analysis.interpolation.LinearInterpolator;
import org.apache.commons.math3.analysis.polynomials.PolynomialSplineFunction;
import org.apache.commons.math3.special.Erf;

import java.util.Arrays;
import java.util.Random;

public class MB {

    // Physics constants
    final static double Pi = 3.1415926;
    final static double kB = 1.38 * Math.pow(10, -23);
    final static double e = 2.71828;
    // mass of a small particle - Argon
    final static double m = 40 * 1.66 * Math.pow(10, -27);

    Random rand = new Random();

    double Velocity;
    double alpha;

    PolynomialSplineFunction psf;
    double[] MB_CDF_SHORT;
    int max_i = 0;

    public MB(int T) {

        int args_nr = 1000; // number of CDF points

        double a = Math.sqrt(kB * T / m);
        double[] v = new double[args_nr];
        double[] MB_CDF = new double[args_nr];

        double[] ff1 = new double[args_nr];
        double[] ff2 = new double[args_nr];

        // calculation of Maxwell-Boltzmann Cumulative distribution function for specified arguments
        for (int i = 0; i < args_nr; i++) {
            v[i] = i;
            ff1[i] = v[i] / (Math.sqrt(2) * a);
            ff2[i] = Math.sqrt(2 / Pi) * v[i] / a * Math.pow(e, -v[i] * v[i] / 2 / a / a);
            MB_CDF[i] = Erf.erf(ff1[i]) - ff2[i];
            if (MB_CDF[i] <= 0.999) max_i = i;
        }

        Arrays.sort(MB_CDF);

        // shortening arrays
        MB_CDF_SHORT = new double[max_i];
        double[] v_short = new double[max_i];
        for (int i = 0; i < max_i; i++) {
            MB_CDF_SHORT[i] = MB_CDF[i];
            v_short[i] = v[i];
        }

        LinearInterpolator li = new LinearInterpolator();
        psf = li.interpolate(MB_CDF_SHORT, v_short);
    }

    public void generateVelocity() {
        Velocity = psf.value(rand.nextDouble() * MB_CDF_SHORT[max_i - 1]);
        alpha = Math.random() * 2 * Math.PI;
    }

    public double setVx() {
        return Velocity * Math.cos(alpha);
    }

    public double setVy() {
        return Velocity * Math.sin(alpha);
    }

}