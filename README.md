# Brownian Motion simulation

This project shows a physics phenomenon called Brownian Motion. It was created as a part of completing the programming course at the Warsaw University of Technology.

## Authors
Konrad Marciniak and Dawid Karpiński
